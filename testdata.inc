<?php

/**
 * @file
 * Classes used by the TestData module.
 */

/**
 * CachedItemList
 *
 * This is essentially a looping array iterator that queries the database to
 * populate, then caches itself along with the current position in the list
 * into the Drupal cache.
 */
class CachedItemList {
  protected $items;
  protected $cache_key;
  protected $sql;
  protected $valid;
  protected $pos;
  protected $sample_limit;
  protected $origin;
  protected $cache_bin;

  /**
   * Constructor.
   *
   * We don't attempt to prepare the contents here, rather do it the first time
   * someone attempts to get some data (lazy loading).
   *
   * @param $cache_key
   *   The unique key to be used for caching this item list.
   * @param $sql
   *   The SQL to use to query for the data from the database. This must have
   *   one alias defined as 'item', e.g. SELECT nid AS item FROM ...
   * @param $sample_limit
   *   Used for the LIMIT clause on the query, the maximum amount of data to
   *   return.
   */
  public function __construct($cache_key, $sql, $sample_limit) {
    $this->cache_key = $cache_key;
    $this->sql = $sql;
    $this->sample_limit = $sample_limit;

    $this->items = array();
    $this->valid = FALSE;
    $this->pos = 0;
    $this->origin = NULL;
    $this->cache_bin = 'cache';
  }

  /**
   * Destructor.
   */
  public function __destruct() {
    // Save this object back to the cache.
    $this->setCache();
  }

  /**
   * Return the current item then increment the position by one, looping if
   * we reach the end of the cache.
   */
  public function next() {
    if (!$this->prepare()) {
      return NULL;
    }

    if ($this->pos >= count($this->items)) {
      $this->pos = 0;
    }
    $output = $this->items[$this->pos];
    $this->pos++;

    return $output;
  }

  /**
   * Prepare by attempting to populate from the cache first, database second.
   */
  public function prepare() {
    if (!$this->valid) {
      // First look in the cache.
      $this->loadDataFromCache();

      if (!$this->valid) {
        // If the cache is empty, query for the data.
        $this->loadDataFromDatabase();

        // Still nothing, so fail.
        if (!$this->valid) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * Get the data from the cache and populate this object.
   */
  public function loadDataFromCache() {
    $cache_object = cache_get('cached-item-list:' . $this->cache_key, $this->cache_bin);

    if (!empty($cache_object->data['items'])) {
      $this->items = $cache_object->data['items'];
      $this->pos = $cache_object->data['pos'];
      $this->origin = 'cache';
      $this->valid = TRUE;
    }
  }

  /**
   * Run the query to grab the data from the database.
   */
  public function loadDataFromDatabase() {
    $items = array();
    $result = db_query_range($this->sql, 0, $this->sample_limit);
    while ($obj = db_fetch_object($result)){
      // 'item' is the magic alias that must be returned by the SQL.
      // E.g. SELECT nid as item ...
      $items[] = $obj->item;
    }

    if (!empty($items)) {
      $this->items = $items;
      $this->pos = 0;
      $this->origin = 'query';
      $this->valid = TRUE;
    }
  }

  /**
   * Cache the current object's items and position.
   */
  public function setCache() {
    if ($this->valid) {
      $cache_item = array(
        'items' => $this->items,
        'pos' => $this->pos,
      );
      cache_set('cached-item-list:' . $this->cache_key, $cache_item, $this->cache_bin, CACHE_PERMANENT);
    }
  }

  /**
   * Destroy the cache item for this object.
   */
  public function clearCache() {
    cache_clear_all('cached-item-list:' . $this->cache_key, $this->cache_bin);
  }
}


/**
 * TestDataList extends CachedItemList.
 *
 * Provides specific functionality for using a CachedItemList as a store for
 * test data, methods like formatting the data as a URL or returning it in JSON
 * format reside here.
 */
class TestDataList extends CachedItemList {
  protected $url_format;

  /**
   * Constructor.
   *
   * @param $url_format
   *   Used to construct a URL out of an item of test data. E.g. 'node/%' as
   *   the url_format will have the '%' substituted for each item, creating
   *   node/1, node/2, node/3, etc.
   */
  public function __construct($cache_key, $sql, $sample_limit, $url_format) {
    parent::__construct($cache_key, $sql, $sample_limit);
    $this->url_format = $url_format;
  }

  /**
   * Return the next item as it's URL equivalent. E.g. 100 into /node/100
   */
  public function nextAsUrl() {
    $item = $this->next();
    if (isset($item)) {
      return str_replace('%', (string)$item, $this->url_format);
    }
    return FALSE;
  }

  /**
   * Return $amount of data as a JSON object.
   */
  public function getJson($amount) {
    if (is_numeric($amount)) {
      $output = array();

      for ($i = 0; $i < $amount; $i++) {
        $item = $this->next();
        if (isset($item)) {
          $output[] = $item;
        }
      }

      if (!empty($output)) {
        return drupal_to_js($output);
      }
    }
    return FALSE;
  }

  /**
   * Return $amount of URL aliases as a JSON object.
   */
  public function getUrlsAsJson($amount) {
    if (is_numeric($amount)) {
      $output = array();

      for ($i = 0; $i < $amount; $i++) {
        $item = drupal_get_path_alias($this->nextAsUrl());
        if (isset($item)) {
          $output[] = $item;
        }
      }

      if (!empty($output)) {
        return drupal_to_js($output);
      }
    }
    return FALSE;
  }

  /**
   * Get a table containing the status of this item.
   */
  public function asTable() {
    if (!$this->prepare()) {
      return t('Unable to read data from cache or query.');
    }
    $header = array(t('Attribute'), t('Value'));
    $rows[] = array(t('Key'), $this->cache_key);
    $rows[] = array(t('Curr pos') , $this->pos);
    $rows[] = array(t('Num items') , count($this->items));
    $rows[] = array(t('Sample limit') , $this->sample_limit);
    $rows[] = array(t('Origin') , $this->origin);
    $rows[] = array(t('Query') , $this->sql);
    $rows[] = array(t('URL format') , $this->url_format);

    return theme('table', $header, $rows);
  }
}

